Test deployment :

```
$ ssh-copy-id vagrant@httpclient
$ fab deploy -H httpclient
```

Deployment :

```
$ fab deploy:server_name=browsertest.quoonel.com -H luc2@browsertest.quoonel.com
```