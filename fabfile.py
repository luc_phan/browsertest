from fabric.api import *
from fabric.contrib.files import exists, upload_template
import pwd
import os

def deploy(server_name='zotac', http_port=80, https_port=443):
    run("cat /etc/system-release")

    _yum_install("ius-release")
    _yum_install("python34u")

#    try:
#        pwd.getpwnam('browsertest')
#    except KeyError:
#        r = run("sudo useradd browsertest")
#    else:
#        print("*** User already added: browsertest")

    _user_add("browsertest")

    _pip3_install("virtualenv")
    _pip3_install("virtualenvwrapper")

    #with settings(sudo_user="browsertest", warn_only=True):
    with settings(sudo_user="browsertest"):
#        r = sudo("grep VIRTUALENVWRAPPER_PYTHON /home/browsertest/.bashrc")
#        if r.succeeded:
#            print("*** Line already added to .bashrc: VIRTUALENVWRAPPER_PYTHON")
#        else:
#            sudo("echo export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 >> /home/browsertest/.bashrc")
#
#        r = sudo("grep virtualenvwrapper.sh /home/browsertest/.bashrc")
#        if r.succeeded:
#            print("*** Line already added to .bashrc: virtualenvwrapper.sh")
#        else:
#            sudo("echo source /usr/bin/virtualenvwrapper.sh >> /home/browsertest/.bashrc")

#        if exists('/home/browsertest/.virtualenvs/local', use_sudo=True):
#            print("*** VirtualEnv already created: local")
#        else:
#            sudo("mkvirtualenv local -p /usr/bin/python3")

        _mkvirtualenv("local")

#        if exists('/home/browsertest/work', use_sudo=True):
#            print("*** Directory already created: work")
#        else:
#            sudo("mkdir /home/browsertest/work")

        _mkdir("/home/browsertest/live")

    local("git archive master | xz > browsertest.tar.xz")
    put("browsertest.tar.xz", "/home/browsertest/live/browsertest.tar.xz", use_sudo=True)

    #with settings(sudo_user="browsertest", warn_only=True):
    with settings(sudo_user="browsertest"):
#        if exists('/home/browsertest/work/browsertest', use_sudo=True):
#            print("*** Directory already created: browsertest")
#        else:
#            sudo("mkdir /home/browsertest/work/browsertest")

        _mkdir("/home/browsertest/live/browsertest")
        sudo("tar xvJf /home/browsertest/live/browsertest.tar.xz -C /home/browsertest/live/browsertest")

        #with prefix("source virtualenvwrapper.sh; workon local"):
        #sudo("source virtualenvwrapper.sh; workon local")
        with prefix('export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 && source virtualenvwrapper.sh && workon local'):
            _pip3_install("django")
            with cd("/home/browsertest/live/browsertest"):
                sudo("python manage.py migrate")
            _pip3_install("uwsgi")

    put("deployment/browsertest.ini", "/home/browsertest/live/browsertest/browsertest.ini", use_sudo=True)
    put("deployment/browsertest.service", "/etc/systemd/system/browsertest.service", use_sudo=True)
    sudo("systemctl daemon-reload")
    sudo("systemctl restart browsertest")
    sudo("systemctl enable browsertest")

    _yum_install("nginx")
    create_certificate()
    #put("deployment/browsertest.conf", "/etc/nginx/conf.d/browsertest.conf", use_sudo=True)
    upload_template(
        "deployment/browsertest.conf",
        "/etc/nginx/conf.d/browsertest.conf",
        context={
            'server_name': server_name,
            'http_port': int(http_port),
            'https_port': int(https_port)
        },
        use_sudo=True
    )
    sudo("usermod -a -G browsertest nginx")
    sudo("chmod g+x /home/browsertest")
    sudo("systemctl restart nginx")
    sudo("systemctl enable nginx")

    with settings(warn_only=True):
        r = sudo("firewall-cmd --state")
    if r.succeeded:
        sudo("firewall-cmd --zone=public --add-port={}/tcp --permanent".format(http_port))
        sudo("firewall-cmd --zone=public --add-port={}/tcp --permanent".format(https_port))
        sudo("firewall-cmd --reload")
        sudo("firewall-cmd --list-ports")
    else:
        print("*** Firewall is not running")

def _yum_install(package):
    with settings(warn_only=True):
        r = run("yum list installed " + package)
    if r.succeeded:
        print("*** Yum package already installed: " + package)
    else:
        #run("sudo yum install " + package)
        sudo("yum install -y " + package)

def _user_add(user):
    if exists(os.path.join('/home',user), use_sudo=True):
        print("*** User already added: " + user)
    else:
        sudo("useradd " + user)

def _pip3_install(package):
#    prefix = "sudo " if sudo else ""
#    r = run(prefix + "pip3 show " + package)
#    if r.succeeded:
#        print("*** Pip3 package already installed: " + package)
#    else:
#        run(prefix + "pip3 install " + package)

    with settings(warn_only=True):
        r = sudo("pip3 show " + package)
    if r.succeeded:
        print("*** Pip3 package already installed: " + package)
    else:
        sudo("pip3 install " + package)

def _mkvirtualenv(env):
    if exists(os.path.join('/home/browsertest/.virtualenvs',env), use_sudo=True):
        print("*** VirtualEnv already created: " + env)
    else:
        with prefix("export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 && source virtualenvwrapper.sh"):
            sudo("mkvirtualenv {} -p /usr/bin/python3".format(env))

def _mkdir(directory, use_sudo=False):
    if exists(directory, use_sudo=True):
        print("*** Directory already created: " + directory)
    else:
        sudo("mkdir " + directory)

def create_certificate():
    _mkdir("/etc/nginx/ssl")
    with cd("/etc/nginx/ssl"):
        if exists("ca.key"):
            print("*** Key already created: /etc/nginx/ssl/ca.key")
        else:
            sudo("openssl genrsa -out ca.key 2048")
        if exists("ca.csr"):
            print("*** Request already created: /etc/nginx/ssl/ca.csr")
        else:
            sudo("openssl req -new -key ca.key -out ca.csr -subj '/'")
        if exists("ca.csr"):
            print("*** Certificate already created: /etc/nginx/ssl/ca.crt")
        else:
            sudo("openssl x509 -req -days 36500 -in ca.csr -signkey ca.key -out ca.crt")
