from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^ip$', views.ip, name='ip'),
    url(r'^user-agent$', views.user_agent, name='user_agent'),
]
