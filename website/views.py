from django.shortcuts import render
from django.http import HttpResponse
import json

# Create your views here.

def index(request):
    return render(request, 'website/index.html')

def ip(request):
    origins = []

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        origins.append(x_forwarded_for)

    origin = request.META.get('REMOTE_ADDR')
    if origin:
        origins.append(origin)

    response = {'origin': ', '.join(origins)}
    return HttpResponse(
        json.dumps(response, indent=2), content_type="application/json"
    )

def user_agent(request):
    response = {'user-agent': request.META.get('HTTP_USER_AGENT')}
    return HttpResponse(
        json.dumps(response, indent=2), content_type="application/json"
    )
